import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

df = pd.read_csv("C:/Users/ptitb/Documents/Unity Projects/Crowd Evacuation/analyse.csv", sep =';', decimal=",")

#df['Mass'] = pd.to_numeric(df['Mass'],errors = 'coerce')
#df['Speed'] = pd.to_numeric(df['Speed'],errors = 'coerce')

#df.Time = df.Time / df.Distance
df = df.sort_values(by='Time')
df = df.reset_index()

df1 = df.query("Type == \"enfant\"")[['Time',"Type","Distance"]]
df1 = df1.reset_index()


df2 = df.query("Type == \"adulte\"")[['Time',"Type","Distance"]]
df2 = df2.reset_index()


df3 = df.query("Type == \"vieux\"")[['Time',"Type","Distance"]]
df3 = df3.reset_index()

#plt.hist([df.Time],bins=25)
#plt.show()


plt.plot(df.Time)
plt.rcParams['figure.figsize'] = [5, 3]
plt.xlabel("id agent")
plt.ylabel("time (s)")
#plt.show()
plt.savefig('C:/Users/ptitb/Documents/Unity Projects/Crowd Evacuation/time.png', bbox_inches="tight")
plt.close()

labels = ['enfant', 'adulte', 'personne âgée']

plt.scatter(df1.Time,df1.Distance,s=5)
plt.scatter(df2.Time,df2.Distance,s=5)
plt.scatter(df3.Time,df3.Distance,s=5)
plt.legend(labels)
plt.rcParams['figure.figsize'] = [5, 3]
plt.ylabel("distance from exit (m)")
plt.xlabel("time (s)")
#plt.show()
plt.savefig('C:/Users/ptitb/Documents/Unity Projects/Crowd Evacuation/time_distance.png', bbox_inches="tight")
plt.close()

plt.hist([df1.Time,df2.Time,df3.Time], density=True, label=labels)
plt.rcParams['figure.figsize'] = [5, 3]
plt.legend(labels)
plt.xlabel("time (s)")
plt.ylabel("% from density")
#plt.show()
plt.savefig('C:/Users/ptitb/Documents/Unity Projects/Crowd Evacuation/type.png', bbox_inches="tight")
plt.close()

"""
plt.scatter(df.Time,df.Mass,s=5)
plt.rcParams['figure.figsize'] = [5, 3]
plt.ylabel("mass (Kg)")
plt.xlabel("time (s)")
plt.show()
#plt.savefig('C:/Users/ptitb/Documents/Unity Projects/Crowd Evacuation/fig2.png')

plt.scatter(df.Time,df.Speed,s=5)
plt.rcParams['figure.figsize'] = [5, 3]
plt.ylabel("speed (m/s)")
plt.xlabel("time (s)")
plt.show()
#plt.savefig('C:/Users/ptitb/Documents/Unity Projects/Crowd Evacuation/fig3.png')

#plt.plot(df3.Time)
"""

df_old = pd.read_csv("C:/Users/ptitb/Documents/Unity Projects/Crowd Evacuation/last_analyse.csv", sep =';', decimal=",")
df_old = df_old.sort_values(by='Time')
df_old = df_old.reset_index()


plt.plot(df.Time)
plt.plot(df_old.Time)
plt.legend(['actual simulation','last simulation'])
plt.rcParams['figure.figsize'] = [5, 3]
plt.xlabel("id agent")
plt.ylabel("time (s)")
plt.savefig('C:/Users/ptitb/Documents/Unity Projects/Crowd Evacuation/current_last.png', bbox_inches="tight")
plt.close()
#plt.show()