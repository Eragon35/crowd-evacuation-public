using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using ABMU.Core;
using ABMU;

using UnityEngine.AI;

using UnityEditor;
using System;
using System.IO;
using System.Diagnostics;

public class Controller : AbstractController
{
    public GameObject agentPrefab;
    public GameObject agentPrefab2;
    public GameObject canvas;
    public GameObject time;


    public List<GameObject> pieces;

    public List<GameObject> exterieur;

    public List<GameObject> sorties;

    int number_exit = 0;

    public bool save = false;
    public bool save_start = false;
    public bool reload_start = false;

    public bool show = false;

    private bool evacuating = false;

    public int numAgents = 100;

    List<GameObject> list_agent = new List<GameObject>();

    private BinaryWriter bw;
    private BinaryReader br;

    private float start_time = 0;
    private float end_time = 0;

    // Start is called before the first frame update
    public override void Init()
    {
        base.Init();

        // on limite le nombre de fps � 60 pour avoir une simulation constante
        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = 60;

        // on limite le nombre de fps � 60 pour avoir une simulation constante

        // pour la d�monstration ( load depuis un fichier )
        if (!show)
        {
            if (reload_start)
            {
                //reading from the file
                try
                {
                    br = new BinaryReader(new FileStream("starting_data", FileMode.Open));
                    numAgents = br.ReadInt32();
                }
                catch (IOException e)
                {
                    Console.WriteLine(e.Message + "\n Cannot open file.");
                    return;
                }
            }

            for (int i = 0; i < numAgents; i++)
            {
                // on initialise les agents
                GameObject agent = Instantiate(agentPrefab);
                list_agent.Add(agent);
                NavMeshAgent nmAgent = agent.transform.GetChild(0).gameObject.GetComponent<NavMeshAgent>();
                agent.transform.GetChild(0).GetComponent<Agent>().Init();

                if (!reload_start)
                {
                    int randomRoom = UnityEngine.Random.Range(0, pieces.Count);
                    agent.transform.GetChild(0).GetComponent<Agent>().Randomize();
                    nmAgent.Warp(GetRandomPointInRoom(pieces[randomRoom]));
                    agent.transform.GetChild(0).GetComponent<Agent>().setRoom(randomRoom);
                }
                else  // si on load la situation initiale
                {
                    nmAgent.Warp(new Vector3(br.ReadSingle(), br.ReadSingle(), br.ReadSingle()));

                    agent.transform.GetChild(0).GetComponent<Agent>().Randomize(br.ReadInt32(), br.ReadSingle(), br.ReadSingle(), br.ReadSingle(), br.ReadSingle(), br.ReadSingle(), br.ReadInt32());
                }

                agent.transform.GetChild(0).GetComponent<Agent>().reference_controller(this);
            }

            // si on veut sauvegarder le point de d�part
            if (save_start && !reload_start)
            {
                //create the file
                try
                {
                    bw = new BinaryWriter(new FileStream("starting_data", FileMode.Create));
                }
                catch (IOException e)
                {
                    Console.WriteLine(e.Message + "\n Cannot create file.");
                    return;
                }

                //writing into the file
                try
                {
                    bw.Write(numAgents);
                }
                catch (IOException e)
                {
                    Console.WriteLine(e.Message + "\n Cannot write to file.");
                    return;
                }

                for (int i = 0; i < list_agent.Count; i++)
                {
                    list_agent[i].transform.GetChild(0).GetComponent<Agent>().Save_start(bw);
                }
            }

            for (int i = 0; i < list_agent.Count; i++)
            {
                list_agent[i].transform.GetChild(0).GetComponent<Agent>().StartWandering(pieces);
            }

            if (save)
            {
                //create the file
                try
                {
                    bw = new BinaryWriter(new FileStream("mydata", FileMode.Create));
                }
                catch (IOException e)
                {
                    Console.WriteLine(e.Message + "\n Cannot create file.");
                    return;
                }

                //writing into the file
                try
                {
                    bw.Write(numAgents);

                    for (int i = 0; i < numAgents; i++)
                    {
                        Agent a = list_agent[i].transform.GetChild(0).GetComponent<Agent>();
                        bw.Write(a.get_width());
                        bw.Write(a.get_height());
                    }
                }
                catch (IOException e)
                {
                    Console.WriteLine(e.Message + "\n Cannot write to file.");
                    return;
                }
            }
        }
        else
        {
            //reading from the file
            try
            {
                br = new BinaryReader(new FileStream("mydata", FileMode.Open));
            }
            catch (IOException e)
            {
                Console.WriteLine(e.Message + "\n Cannot open file.");
                return;
            }



            try
            {
                numAgents = br.ReadInt32();
            }
            catch (IOException e)
            {
                Console.WriteLine(e.Message + "\n Cannot read from file.");
                return;
            }

            for (int i = 0; i < numAgents; i++)
            {

                GameObject agent = Instantiate(agentPrefab2);
                float width = br.ReadSingle();
                float height = br.ReadSingle();
                agent.transform.GetChild(0).transform.localScale = new Vector3(
                                                                                width,
                                                                                height,
                                                                                width);
                list_agent.Add(agent);

            }
        }
        


    }

    public void addExit()
    {
        // fonction qui permet de d�compter le nombre d'agents ayant r�ussi l'�vacuation
        number_exit++;
        if (number_exit >= numAgents) end();
    }

    private void end()
    {
        File.Delete("last_analyse.csv");
        File.Move("analyse.csv", "last_analyse.csv");

        end_time = Time.time;

        // on sauvegarde les donn�es statistiques de la simulation

        string filePath = "analyse.csv";

        StreamWriter writer = new StreamWriter(filePath);

        writer.WriteLine("Time;Type;Mass;Speed;Distance");

        for (int i = 0; i < list_agent.Count; i++)
        {
            writer.WriteLine(list_agent[i].transform.GetChild(0).GetComponent<Agent>().ToCsv());
        }
        writer.Close();

        // on lance le script python permettant de g�nerer les graphiques

        string fileName = @"C:/Users/ptitb/PycharmProjects/analyse_crowd/main.py";

        Process p = new Process();
        p.StartInfo = new ProcessStartInfo(@"C:/Users/ptitb/PycharmProjects/analyse_crowd/venv/Scripts/python.exe", fileName)
        {
            RedirectStandardOutput = true,
            UseShellExecute = false,
            CreateNoWindow = true
        };
        p.Start();

        string output = p.StandardOutput.ReadToEnd();
        p.WaitForExit();

        Console.WriteLine(output);

        Console.ReadLine();
        time.GetComponent<Text>().text = ((int) (end_time - start_time)).ToString() + " seconds";

        // on afficher le canvas des r�ultats
        canvas.SetActive(true);
    }

    private void Save()
    {
            //writing into the file
            try
            {
                for(int i=0; i<numAgents; i++)
                {
                    // sauvegarde de toutes les informations importantes � chaque frame
                    bw.Write(list_agent[i].transform.GetChild(0).GetComponent<Rigidbody>().position.x);
                    bw.Write(list_agent[i].transform.GetChild(0).GetComponent<Rigidbody>().position.y);
                    bw.Write(list_agent[i].transform.GetChild(0).GetComponent<Rigidbody>().position.z);
                    bw.Write(list_agent[i].transform.GetChild(0).GetComponent<Renderer>().material.color.r);
                }

            }
            catch (IOException e)
            {
                Console.WriteLine(e.Message + "\n Cannot write to file.");
                return;
            }
    }
    // Update is called once per frame
    void Update()
    {
        

        if (show)
        {
            try
            {
                for (int i = 0; i < numAgents; i++)
                {
                    //r�cup�ration des informations dans le fichier pour l'affichage
                    float x = br.ReadSingle();
                    float y = br.ReadSingle();
                    float z = br.ReadSingle();
                    float r = br.ReadSingle();
                    list_agent[i].transform.GetChild(0).transform.position = new Vector3(x, y, z);
                    list_agent[i].transform.GetChild(0).GetComponent<Renderer>().material.color = new Color(r, 0, 0);
                }


            }
            catch (IOException e)
            {
                Console.WriteLine(e.Message + "\n Cannot read from file.");
                return;
            }
        }else if (save)
        {
            Save();
        }
    }

    void OnGUI()
    {
        // d�tection de l'appui de la touche espace, pour lancer l'�vacuation
        if (Event.current.Equals(Event.KeyboardEvent("Space")))
        {
            if (!evacuating)
            {
                evacuating = true;

                UnityEngine.Debug.Log("Spacebar");
                start_time = Time.time;

                for (int i = 0; i < list_agent.Count; i++)
                {
                    list_agent[i].transform.GetChild(0).gameObject.GetComponent<Agent>().Escape(sorties, exterieur);
                }
            }
            

        }

    }

    void OnDestroy()
    {
        // on ferme les fichiers � la fin de la simulation
        if (save)
        {
            bw.Close();
        }

        if (show)
        {
            br.Close();
        }
    }


    public static Vector3 GetRandomPointInRoom(GameObject room)
    {
        // fonction permettant de g�n�rer un point al�atoire dans une pi�ce du b�timent.

        Mesh planeMesh = room.GetComponent<MeshFilter>().mesh;
        Bounds bounds = planeMesh.bounds;

        float boundsX = room.transform.localScale.x * bounds.size.x;
        float boundsZ = room.transform.localScale.z * bounds.size.z;

        float randomX = UnityEngine.Random.Range(room.transform.position.x + bounds.center.x - boundsX / 2 + 0.7f, room.transform.position.x + bounds.center.x + boundsX / 2 - 0.7f);
        float randomZ = UnityEngine.Random.Range(room.transform.position.z + bounds.center.z - boundsZ / 2 + 0.7f, room.transform.position.z + bounds.center.z + boundsZ / 2 - 0.7f);

        /*float randomX = UnityEngine.Random.Range(room.transform.position.x - room.transform.localScale.x / 2, room.transform.position.x + room.transform.localScale.x / 2);
        float y = room.transform.position.y + 1.51f;
        float randomZ = UnityEngine.Random.Range(room.transform.position.y - room.transform.localScale.z / 2, room.transform.position.y + room.transform.localScale.z / 2);*/

        float y = room.transform.position.y + bounds.center.y;
        y += bounds.size.y / 2;
        //y += +1.51f;

        Vector3 pos = new Vector3(randomX, y, randomZ);

        return pos;
    }

}
