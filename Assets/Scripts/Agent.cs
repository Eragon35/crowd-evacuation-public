using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ABMU.Core;
using UnityEngine.AI;
using UnityEditor;
using ABMU;
using System.IO;

public class Agent : AbstractAgent
{
    Controller controller;

    NavMeshAgent nmAgent;
    Rigidbody Rigidbody;
    Renderer Render;

    List<GameObject> exterieur;
    List<GameObject> sorties;

    int selected_exit = -1;
    int selected_room = -1;

    List<GameObject> pieces;

    Vector3 target;

    bool escaping = false;

    bool moving = false;
    bool am_out = false;

    List<float> forces = new List<float>();

    int reset = 0;

    Color baseColor = Color.black;

    float agent_width = 0.275f;
    float agent_height = 0.8f;

    // size minimum : 0.35 0.6 0.35
    // size maximum : 0.7 1.1 0.7

    float t_start;
    float t_end;

    string type = "";

    float distance = -1;
    public override void Init()
    {
        // initialisation des diff�rents composants des agents.
        base.Init();
        nmAgent = GetComponent<NavMeshAgent>();
        Rigidbody = GetComponent<Rigidbody>();
        Render = GetComponent<Renderer>();


        nmAgent.isStopped = true;
        nmAgent.updatePosition = true;
        nmAgent.updateRotation = true;

    }

    public void Randomize()
    {
        // g�n�ration al�atoire des agents.

        int random_type = UnityEngine.Random.Range(0, 12);

        if (random_type < 2) // enfant
        {
            float width = UnityEngine.Random.Range(0.3f, 0.4f);
            agent_width = width;
            agent_height = UnityEngine.Random.Range(0.6f, 0.75f);
            this.transform.localScale = new Vector3(
                width,
                agent_height,
                width);
            width += 0.05f;
            Rigidbody.mass = width * width * width * 500; // 21.4 - 45.5
            nmAgent.speed = UnityEngine.Random.Range(2.5f, 3.5f);
            nmAgent.acceleration = UnityEngine.Random.Range(6f, 9f);
            //baseColor.g = 1f;
            type = "enfant";
        }
        else if (random_type < 10)// adulte
        {
            float width = UnityEngine.Random.Range(0.4f, 0.6f);
            agent_width = width;
            agent_height = UnityEngine.Random.Range(0.7f, 1.1f);
            this.transform.localScale = new Vector3(
                width,
                agent_height,
                width);
            width += 0.1f;
            Rigidbody.mass = width * width * width * 400; // 36.45 - 137.2
            nmAgent.speed = UnityEngine.Random.Range(2f, 3f);
            nmAgent.acceleration = UnityEngine.Random.Range(5f, 7f);
            type = "adulte";
        }
        else // vieux
        {
            float width = UnityEngine.Random.Range(0.4f, 0.5f);
            agent_width = width;
            agent_height = UnityEngine.Random.Range(0.75f, 0.95f);
            this.transform.localScale = new Vector3(
                width,
                agent_height,
                width);
            width += 0.075f;
            Rigidbody.mass = width * width * width * 400; // 36.45 - 90
            nmAgent.speed = UnityEngine.Random.Range(1f, 1.5f);
            nmAgent.acceleration = UnityEngine.Random.Range(2f, 4f);
            //baseColor.b = 1f;
            type = "vieux";
        }

    }

    public float get_width()
    {
        return this.agent_width;
    }

    public float get_height()
    {
        return this.agent_height;
    }

    public void Randomize(int type, float width, float height, float mass, float speed, float acceleration, int room)
    {
        // r�cup�ration des donn�es g�n�r�es par une autre simulation

        switch (type)
        {
            case 0:
                this.type = "enfant";
                break;
            case 1:
                this.type = "adulte";
                break;
            case 2:
                this.type = "vieux";
                break;
            default:
                this.type = "adulte";
                break;
        }
        this.transform.localScale = new Vector3(
                width,
                height,
                width);
        Rigidbody.mass = mass;
        nmAgent.speed = speed;
        nmAgent.acceleration = acceleration;
        selected_room = room;
    }

    public void Save_start(BinaryWriter bw) {
        // sauvegarde des valeurs g�n�r�es al�atoirement

        bw.Write(this.transform.position.x);
        bw.Write(this.transform.position.y);
        bw.Write(this.transform.position.z);

        switch (this.type)
        {
            case "enfant":
                bw.Write(0);
                break;
            case "adulte":
                bw.Write(1);
                break;
            case "vieux":
                bw.Write(2);
                break;
            default:
                bw.Write(1);
                break;
        }

        bw.Write(this.agent_width);
        bw.Write(this.agent_height);
        bw.Write(this.Rigidbody.mass);
        bw.Write(this.nmAgent.speed);
        bw.Write(this.nmAgent.acceleration);
        bw.Write(this.selected_room);

    }

    public void reference_controller(Controller controller)
    {
        this.controller = controller;
    }
    public void setRoom(int selected_room)
    {
        this.selected_room = selected_room;
    }

    public void StartWandering(List<GameObject> pieces)
    {
        // fonction permettant de lancer le comportement de base de l'agent.

        this.pieces = pieces;

        int what_to_do = UnityEngine.Random.Range(0, 10);

        if(what_to_do < 8)
        {
            target = GetRandomPointInRoom(this.pieces[selected_room]);
            
        }
        else
        {
            int randomRoom = UnityEngine.Random.Range(0, this.pieces.Count);
            selected_room = randomRoom;
            target = GetRandomPointInRoom(this.pieces[randomRoom]);
        }
        nmAgent.SetDestination(target);
        nmAgent.isStopped = false;
        moving = true;
        CreateStepper(CheckDistToTarget, 2, 95);
        CreateStepper(FrameUpdate, 1, 120);

    }

    void RestartWandering()
    {
        // fonction permettant de relancer le comportement de base de l'agent.
        if (!escaping)
        {
            int what_to_do = UnityEngine.Random.Range(1, 10);

            if (what_to_do < 8)
            {
                target = GetRandomPointInRoom(this.pieces[selected_room]);

            }
            else
            {
                int randomRoom = UnityEngine.Random.Range(0, this.pieces.Count);
                selected_room = randomRoom;
                target = GetRandomPointInRoom(this.pieces[randomRoom]);
            }
            nmAgent.SetDestination(target);
            nmAgent.isStopped = false;
            moving = true;
            CreateStepper(CheckDistToTarget, 2, 95);
        }

    }



    public void Escape(List<GameObject> sorties, List<GameObject> exterieur)
    {
        // fonction permettant de lancer l'�vacuation.
        t_start = Time.time;
        this.sorties = sorties;
        this.exterieur = exterieur;

        am_out = false;

        SelectTarget();

        nmAgent.isStopped = false;
        escaping = true;

        if (!moving)
        {
            CreateStepper(CheckDistToTarget, 2, 95);
        }
        moving = true;
        CreateStepper(SelectTarget, 5, 100);
        
    }
    void CheckDistToTarget()
    {
        // fonction permettant de v�rifier que l'agent est arriv� � destination.

        if (!nmAgent.isStopped)
        {
            float d = Vector3.Distance(this.transform.position, target);
            //Debug.Log("pos :" + this.transform.position + "  target :" + target + "   nm_target :"+ nmAgent.destination + "   distance :"+d);

            if (d < 2)
            { //reached target
                moving = false;
                if (escaping)
                {   
                    if (am_out)
                    {
                        nmAgent.isStopped = true;

                        DestroyStepper("CheckDistToTarget");
                    }
                    else
                    {
                        t_end = Time.time;
                        controller.addExit();
                        am_out = true;
                        DestroyStepper("SelectTarget");
                        SelectTarget();
                    }
                   
                }
                else
                {
                    nmAgent.isStopped = true;

                    DestroyStepper("CheckDistToTarget");

                    int time = Random.Range(4, 25); // time seconds
                    Invoke("RestartWandering", time);
                    //CreateStepper(RestartWandering, time, 90);
                }
            }


        }

    }
    void SelectTarget()
    {
        // fonction permettant de s�lectionnner la sortie la plus proche.
        if (!am_out)
        {

            List<float> distances = new List<float>();
            for (int i = 0; i < sorties.Count; i++)
            {
                Vector3 posit = sorties[i].transform.position;
                posit.Scale(sorties[i].transform.localScale);

                distances.Add(Vector3.Distance(this.transform.position, posit));
            }

            int index_nearest_exit = GetIndexOfLowestValue(distances);

            Vector3 posi = sorties[index_nearest_exit].transform.position;
            posi.Scale(sorties[index_nearest_exit].transform.localScale);

            if (target != posi)
            {

                target = posi;
                nmAgent.SetDestination(target);

            }
        }
        else
        {
            List<float> distances = new List<float>();
            for (int i = 0; i < exterieur.Count; i++)
            {
                distances.Add(Vector3.Distance(this.transform.position, GetCenterOfRoom(exterieur[i])));
            }

            int index_nearest_exit = GetIndexOfLowestValue(distances);

            if (selected_exit != index_nearest_exit)
            {
                target = GetRandomPointInRoom(exterieur[index_nearest_exit]);
                selected_exit = index_nearest_exit;
                nmAgent.SetDestination(target);
            }
        }
        
        
    }

    void FrameUpdate()
    {
        if(distance == -1 && escaping)
        {
            this.distance = GetPathRemainingDistance(); 
        }
        //float d = Vector3.Distance(nmAgent.desiredVelocity, Rigidbody.velocity);
        //Vector3 nextPosition = this.transform.position + nmAgent.desiredVelocity * 0.03f;
        //transform.LookAt(nextPosition, Vector3.up);


        // on v�rifier si l'agent est en contact avec d'autres agents. Pour pouvoir appliquer l'indicateur de forces.
        Collider[] hitColliders = Physics.OverlapSphere(Rigidbody.position, agent_width/2);
        if(hitColliders.Length > 2)
        {
           
            // On fait une moyenne des forces sur plusieurs frames pour r�duire le bruit.
            float force = 0;
            int count = 0;
            foreach (var f in forces)
            {
                force += f;
                count++;
            }
            if (count > 0)
                force /= count;
            Render.material.color = new Color(0.3f * force, 0, 0);
            
        }
        else
        {
            Render.material.color = baseColor;
        }
        
  
        reset++;
        if(reset > 2)
        {
            reset = 0;
            forces.Clear();
        }
    }
    void FixedUpdate()
    {
        // fonction s'executant � chaque update du syst�me de physique de Unity.
        // On Overwrite les donn�es du NavMeshAgent et du RigidBody pour pouvoir les faire fonctionner ensemble

        nmAgent.velocity = Vector3.zero;
        forces.Add(Rigidbody.velocity.magnitude);
        if (nmAgent.pathPending)
        {
            Rigidbody.velocity = Vector3.zero;
        }
        else
        {
            Rigidbody.velocity = nmAgent.desiredVelocity;
        }
    }

    public static int GetIndexOfLowestValue(List<float> arr)
    {
        // fonction qui permet de trouver l'index de l'�l�ment le plus petit d'une liste. (index : emplacement de cet �l�ment)

        float value = float.PositiveInfinity;
        int index = -1;
        for (int i = 0; i < arr.Count; i++)
        {
            if (arr[i] < value)
            {
                index = i;
                value = arr[i];
            }
        }
        return index;
    }

    public static Vector3 GetRandomPointInRoom(GameObject room)
    {
        // fonction permettant de g�n�rer un point al�atoire dans une pi�ce du b�timent.

        Mesh planeMesh = room.GetComponent<MeshFilter>().mesh;
        Bounds bounds = planeMesh.bounds;

        float boundsX = room.transform.localScale.x * bounds.size.x;
        float boundsZ = room.transform.localScale.z * bounds.size.z;

        float randomX = UnityEngine.Random.Range(room.transform.position.x + bounds.center.x - boundsX / 2 + 0.7f, room.transform.position.x + bounds.center.x + boundsX / 2 - 0.7f);
        float randomZ = UnityEngine.Random.Range(room.transform.position.z + bounds.center.z - boundsZ / 2 + 0.7f, room.transform.position.z + bounds.center.z + boundsZ / 2 - 0.7f);

        float y = room.transform.position.y + bounds.center.y ;
        y += bounds.size.y / 2;
        //y += +1.51f;

        Vector3 pos = new Vector3(randomX, y, randomZ);

        return pos;
    }

    public static Vector3 GetCenterOfRoom(GameObject room)
    {
        Mesh planeMesh = room.GetComponent<MeshFilter>().mesh;
        Bounds bounds = planeMesh.bounds;
        float x = (bounds.center.x + room.transform.position.x) * room.transform.localScale.x;
        float y = (bounds.center.y + room.transform.position.y) * room.transform.localScale.y;
        float z = (bounds.center.z + room.transform.position.z) * room.transform.localScale.z;

        return new Vector3(x, y, z);
    }

    public string ToCsv()
    {
        // conversion des donn�es au format csv
        int escape_time = (int)(t_end - t_start);
        string retour = escape_time + ";" + type + ";" + Rigidbody.mass + ";" + nmAgent.speed + ";" + this.distance;

        return retour;
    }

    public float GetPathRemainingDistance()
    {
        // fonction permettant de calculer la distance totale du trajet que les agents devront parcourir.

        if (nmAgent.pathPending ||
            nmAgent.pathStatus == NavMeshPathStatus.PathInvalid ||
            nmAgent.path.corners.Length == 0)
            return -1f;

        float d = 0.0f;
        for (int i = 0; i < nmAgent.path.corners.Length - 1; ++i)
        {
            d += Vector3.Distance(nmAgent.path.corners[i], nmAgent.path.corners[i + 1]);
        }

        return d;
    }
}
