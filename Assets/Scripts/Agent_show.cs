using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Agent_show : MonoBehaviour
{
    List<Vector3> positions = new List<Vector3>();
    int i = 0;
    int size = 0;

    public void add_position(Vector3 pos)
    {
        positions.Add(pos);
        size++;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (i < size)
        {
           // Debug.Log(positions[i]);
            this.transform.position = positions[i];
            i++;
        }
    }
}
