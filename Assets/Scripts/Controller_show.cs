using System.Collections;
using System.Collections.Generic;
using UnityEngine;



using UnityEditor;
using System;
using System.IO;


// /!\ Ce fichier n'est plus utils�, tout est maintenant fait dans un seul controlleur.

public class Controller_show : MonoBehaviour
{
    public GameObject agentPrefab2;

    List<GameObject> list_agent = new List<GameObject>();

    private int numAgents = 1;

    private BinaryReader br;
    // Start is called before the first frame update
    void Start()
    {
        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = 60;

        //reading from the file
        try
        {
            br = new BinaryReader(new FileStream("mydata", FileMode.Open));
        }
        catch (IOException e)
        {
            Console.WriteLine(e.Message + "\n Cannot open file.");
            return;
        }



        try
        {
            numAgents = br.ReadInt32();
        }
        catch (IOException e)
        {
            Console.WriteLine(e.Message + "\n Cannot read from file.");
            return;
        }

        for (int i = 0; i < numAgents; i++)
        {

            GameObject agent = Instantiate(agentPrefab2);
            float width = br.ReadSingle();
            float height = br.ReadSingle();
            agent.transform.GetChild(0).transform.localScale = new Vector3(
                                                                            width,
                                                                            height,
                                                                            width);
            list_agent.Add(agent);
            
        }

        /*while (br.BaseStream.Position != br.BaseStream.Length)
        {
            for (int i = 0; i < numAgents; i++)
            {
                float x = br.ReadSingle();
                float y = br.ReadSingle();
                float z = br.ReadSingle();
                float r = br.ReadSingle();
                //if(i < 1)
                //{
                list_agent[i].transform.GetChild(0).GetComponent<Agent_show>().add_position(new Vector3(x, y, z));
                //}
            }
        }*/

    }

    private void Update()
    {
        try
        {
            for (int i = 0; i < numAgents; i++)
            {
                float x = br.ReadSingle();
                float y = br.ReadSingle();
                float z = br.ReadSingle();
                float r = br.ReadSingle();
                list_agent[i].transform.GetChild(0).transform.position = new Vector3(x, y, z);
                list_agent[i].transform.GetChild(0).GetComponent<Renderer>().material.color = new Color(r,0,0);
            }


        }
        catch (IOException e)
        {
            Console.WriteLine(e.Message + "\n Cannot read from file.");
            return;
        }
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        /*
        try
        {
            for(int i=0; i< numAgents; i++)
            {
                float x = br.ReadSingle();
                float y = br.ReadSingle();
                float z = br.ReadSingle();
                list_agent[i].transform.GetChild(0).transform.position = new Vector3(x, y, z);
            }
           

        }
        catch (IOException e)
        {
            Console.WriteLine(e.Message + "\n Cannot read from file.");
            return;
        }*/
    }

    // Detects if the shift key was pressed
    void OnGUI()
    {

    }

    void OnDestroy()
    {
        br.Close();
        Debug.Log("OnDestroy1");
    }

}
