using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class canvas_controller : MonoBehaviour
{
    private Vector3[] newVertices;
    [SerializeField]
    public GameObject graphTime;
    public GameObject graphTimeDistance;
    public GameObject graphType;
    public GameObject graphLast;


    IEnumerator Start()
    {
        // r�cup�ration des images g�n�r�s par le script python

        WWW www = new WWW("file:///time.png");
        while (!www.isDone)
            yield return null;
        graphTime.GetComponent<RawImage>().texture = www.texture;

        www = new WWW("file:///time_distance.png");
        while (!www.isDone)
            yield return null;
        graphTimeDistance.GetComponent<RawImage>().texture = www.texture;

        www = new WWW("file:///type.png");
        while (!www.isDone)
            yield return null;
        graphType.GetComponent<RawImage>().texture = www.texture;

        www = new WWW("file:///current_last.png");
        while (!www.isDone)
            yield return null;
        graphLast.GetComponent<RawImage>().texture = www.texture;
    }


    // Update is called once per frame
    void Update()
    {
        
    }
}
